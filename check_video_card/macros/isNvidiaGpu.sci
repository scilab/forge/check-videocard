// Copyright 2010 - DIGITEO - Allan CORNET
//

function b = isNvidiaGpu()
  b = %f;
  vc = getVideoCard();
  if grep(vc,'/\bNVIDIA/i','r') <> [] then
    b = %t;
  end

endfunction