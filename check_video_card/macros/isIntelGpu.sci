// Copyright 2010 - DIGITEO - Allan CORNET
//
function b = isIntelGpu()
  b = %f;
  vc = getVideoCard();
  if grep(vc,'/\bINTEL/i','r') <> [] then
    b = %t;
  end

endfunction