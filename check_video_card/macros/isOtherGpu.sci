// Copyright 2010 - DIGITEO - Allan CORNET
//

function b = isOtherGpu()
  b = %f;
  if isAtiGpu() | isNvidiaGpu() | isIntelGpu() | isMatroxGpu() then
    b = %f;
  else
    b = %t;
  end
endfunction
