//-----------------------------------------------------------------------------
// Copyright 2010 - DIGITEO - Allan CORNET
//-----------------------------------------------------------------------------
function b = check_videocard_drivers(os, manufacturer, model, drivers_version);
  b = %f;
  rhs = argn(2);
  if (rhs <> 4 & rhs <> 0) then
    error(999, 'error ...');
  else
    if ~isdef('os') then
      if is2k() | isXP() then
        os = '2K_XP';
      end
      
      if isVista() then
        os = 'VISTA';
      end

      if isSeven() then
        os = 'SEVEN';
      end
    end
    
    if ~isdef('manufacturer') then
      manufacturer = 'OTHERS';
      
      if isAtiGpu() then
        manufacturer = 'ATI';
      end
      if isNvidiaGpu() then
        manufacturer = 'NVIDIA';
      end
      if isIntelGpu() then
        manufacturer = 'INTEL';
      end
      if isMatroxGpu() then
        manufacturer = 'MATROX';
      end
      if isOtherGpu() then
        manufacturer = 'OTHERS';
      end
    end
    
    if ~isdef('model') then
      model = getVideoCard();
    end
    
    if ~isdef('drivers_version') then
      current_version = strcat(string(getVideoDriversVersion()),' ');
    else
      current_version = strcat(string(drivers_version),' ');
    end
  end
  
  if model == '' then
     disp('Video card not detected by this script.');
     if MSDOS then
       disp_unknown_videocard(os, manufacturer, model, drivers_version);
     end
  else
    // Microsoft - WDDM drivers always need to be updated (generic)
    // Microsoft - WDDM drivers installed by default by Microsoft
    // no hardware acceleration
    if isDefaultWindowsDriver() then
      disp_url_for_download(os, manufacturer, 'DEFAULT', current_version);
    else
      supported_version = getVersionSupported(os, manufacturer, model);
      if compareVersion( [0 0 0 0] , supported_version) == 0 then
        disp_unknown_videocard(os, manufacturer, model, drivers_version);
      else
        if compareVersion( evstr(current_version), supported_version) >= 0 then
          b = %t;
        else
          disp_url_for_download(os, manufacturer, model, current_version);
        end
      end
    end
  end
endfunction
//-----------------------------------------------------------------------------
function min_version = getVersionSupported(os, manufacturer, model)
  min_version = [];
  datas = read_datas_file();
  if datas <> [] then
    pos = grep(datas, os + '|' + manufacturer + '|' + model);
    if pos <> [] then
      splitted = tokens(datas(pos(1)),'|');
      if size(splitted,'*') == 5 then
        min_version = evstr(splitted(4));
      end
    else
      pos = grep(datas, os + '|' + manufacturer + '|' + 'DEFAULT');
      if pos <> [] then
        splitted = tokens(datas(pos(1)),'|');
        if size(splitted,'*') == 5 then
          min_version = evstr(splitted(4));
        end
      end
    end
  end
endfunction
//-----------------------------------------------------------------------------
function url = get_url_for_download(os, manufacturer, model, drivers_version)
  url = '';
  datas = read_datas_file();
  if datas <> [] then
    pos = grep(datas, os + '|' + manufacturer + '|' + model + '|' + drivers_version);
    if pos == [] then
      pos = grep(datas, os + '|' + manufacturer + '|' + model);
      if pos == [] then
        pos = grep(datas, os + '|' + manufacturer + '|' + 'DEFAULT');
        if pos <> [] then
          splitted = tokens(datas(pos(1)),'|');
          if size(splitted,'*') == 5 then
            url = splitted(5);
          end
        end
      else
        splitted = tokens(datas(pos(1)),'|');
        if size(splitted,'*') == 5 then
          url = splitted(5);
        end
      end
    else
      splitted = tokens(datas(pos(1)),'|');
      if size(splitted,'*') == 5 then
        url = splitted(5);
      end
    end
  else
    splitted = tokens(datas(pos(1)),'|');
    if size(splitted,'*') == 5 then
      url = splitted(5);
    end
  end
endfunction  
//-----------------------------------------------------------------------------
function disp_unknown_videocard(os, manufacturer, model, drivers_version)
  disp('Please send a email at : scilab.support@scilab.org');
  disp('Subject : check_videocard_capabilities toolbox');
  disp('and put these information :');
  disp('O.S: ' + os);
  disp('manufacturer: ' + manufacturer);
  disp('model: ' + model);
  disp('drivers_version: ' + strcat(string(drivers_version),' '));
endfunction
//-----------------------------------------------------------------------------
function disp_url_for_download(os, manufacturer, model, drivers_version)
  url = get_url_for_download(os, manufacturer, model, drivers_version);
  if url == '' then
    disp('Please contact your Video card manufacturer and download lastest drivers.');
  else
    disp('Please download lastest video drivers: ');
    disp(get_url_for_download(os, manufacturer, model, drivers_version));
  end
endfunction
//-----------------------------------------------------------------------------
function datas = read_datas_file()
  datas = [];
  str_lib = string(check_video_cardlib);
  path_data = str_lib(1) + '../datas';
  data_file = path_data + '/versions-videocards.txt';
  if isfile(data_file) then
    datas = mgetl(data_file);
    // remove comments
    datas(grep(datas, ';')) = [];
  end
endfunction
//-----------------------------------------------------------------------------
function r = compareVersion(ver1, ver2)
  // homogenize version size
  s1 = size(ver1,'*');
  s2 = size(ver2,'*');

  if s1 > 4 then
    ver1 = ver1(1:4);
  end

  if s2 > 4 then
    ver2 = ver2(1:4);
  end
 
  if s1 <> 4 then
    ver1($+1:4) = 0;
  end

  if s2 <> 4 then
    ver2($+1:4) = 0;
  end
  
  // compare
  r = -1;

 if and(ver1 == ver2) then
   r = 0;
 else
   if ver1(1) == ver2(1) then
     if ver1(2) == ver2(2) then
       if ver1(3) == ver2(3) then
         if ver1(4) == ver2(4) then
           r = 0;
         else
           if ver1(4) > ver2(4) then
             r = 1;
           else
             r = -1;
           end
         end
       else
         if ver1(3) > ver2(3) then
          r = 1;
         else
          r = -1
         end
       end
     else
      if ver1(2) > ver2(2) then
        r = 1;
      else
        r = -1;
      end
     end
   else
     if ver1(1) > ver2(1) then
       r = 1;
     else
       r = -1;
     end
   end
 end

endfunction
//-----------------------------------------------------------------------------
function b = isXP()
  b = %f;
  [VerMaj, VerMin] = getos();
  if VerMaj == 'Windows' then
    if grep(VerMin, 'XP') <> [] then
      b = %t;
    end
  end
endfunction
//-----------------------------------------------------------------------------
function b = is2k()
  b = %f;
  [VerMaj, VerMin] = getos();
  if VerMaj == 'Windows' then
    if grep(VerMin, '2000') <> [] then
      b = %t;
    end
  end
endfunction
//-----------------------------------------------------------------------------
function b = isVista()
  b = %f;
  [VerMaj, VerMin] = getos();
  if VerMaj == 'Windows' then
    if grep(VerMin, 'Vista') <> [] then
      b = %t;
    end
  end
endfunction
//-----------------------------------------------------------------------------
function b = isSeven()
  b = %f;
  [VerMaj, VerMin] = getos();
  if VerMaj == 'Windows' then
    if grep(VerMin, 'Seven') <> [] then
      b = %t;
    end
  end
endfunction
//-----------------------------------------------------------------------------
