// Copyright 2010 - DIGITEO - Allan CORNET
//
function v = getVideoDriversVersion()
  v = [];
  if MSDOS then
    info = getdebuginfo();
    msg = strsplit(_("Video card driver version: %s"),':');
    p = grep(info, msg);
    line_version = info(p(1));
    version_vc = strsplit(line_version, ':');
    vstr = version_vc(2);
    if vstr == 'ERROR' then
      vstr = [];
    else
      v = strsplit(vstr, '.');
      v = evstr(v);
      v = v';
    end 
  end
endfunction
