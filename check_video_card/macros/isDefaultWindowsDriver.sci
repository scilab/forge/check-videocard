// Copyright 2010 - DIGITEO - Allan CORNET
//

function b = isDefaultWindowsDriver()
  b = %f;
  vc = getVideoCard();
  if grep(vc,'/\bMicrosoft/i','r') <> [] then
    b = %t;
  end
endfunction
